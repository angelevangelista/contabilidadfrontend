import React, { useState, useEffect } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { useForm, Controller } from "react-hook-form";
import { Typeahead } from 'react-bootstrap-typeahead';
import { GetListAccount, UpdateAsiento } from '../TipoAsientos/Peticiones'





import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';



const ModalEdit = ({ values, toggleActivate, refresh }) => {
    const [sweetalert, setsweetalert] = useState({
        alert: null,
        show: false,
        basicTitle: '',
        basicType: "default"
    })
    const [options, setOptions] = useState([]);



    useEffect(() => {
        async function GetData() {
            var List = await GetListAccount()
            if (List != null) {
                setOptions(List)
            }
        }
        GetData();
    }, []);



    const { register, handleSubmit, errors, control } = useForm();

    const onSubmit = campos => {
        if (campos !== '') {

            campos.ID = values.id;
            UpdateAsiento(campos).then(function (result) {
                if (result === true) {
                    toggleActivate()
                    setsweetalert({ show: true, basicType: 'success', basicTitle: 'Éxito' })
                } else {
                    setsweetalert({ show: true, basicType: 'danger', basicTitle: 'Error' })

                }
            })

        } else {
            errors.showMessages();
        }
    };

    const Confirm = () => {
        debugger
        if (sweetalert.basicType === 'success') {
            refresh()
            setsweetalert({ ...sweetalert, show: false })

        } else {
            setsweetalert({ ...sweetalert, show: false })
        }
    }



    return (
        <div>
            <SweetAlert
                show={sweetalert.show}
                type={sweetalert.basicType}
                title={sweetalert.basicTitle}
                onConfirm={() => Confirm()}
            >
            </SweetAlert >
            <Modal isOpen={values.Estado} toggle={() => toggleActivate()} >
                <form className="needs-validation theme-form" noValidate="" onSubmit={handleSubmit(onSubmit)}>
                    <ModalHeader toggle={() => toggleActivate()}>Editar Asiento</ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-md-9">
                                <div className="form-group form-row" >
                                    <div className="checkbox">
                                        <input type="checkbox" id="checkbox" name="Estado" defaultChecked={values.data.estado} ref={register} />
                                        <label htmlFor="checkbox">Activado</label>
                                    </div>
                                </div>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Nombre</label>
                                    <input className="form-control col-sm-8" name="NombreTipoAsiento" defaultValue={values.data.tipoAsientoNombre} type="text" ref={register({ required: true })} />
                                    <span>{errors.NombreTipoAsiento && 'Favor, introduzca el nombre del asiento.'}</span>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Cuentas del asiento</label>
                                    <div className="col-sm-12 col-xl-9">
                                        <div id="prefetch">
                                            <Controller
                                                as={Typeahead}
                                                control={control}
                                                name="Cuentas"
                                                id="multiple-typeahead"
                                                clearButton
                                                multiple
                                                labelKey="cuentaNombre"
                                                options={options}
                                                placeholder="Cuentas..."
                                                defaultSelected={values.Estado ? options.filter(function (x) { return values.data.cuentasContables.includes(x.cuentasContablesID) }) : []}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">Guardar cambios</Button>
                        <Button color="secondary" onClick={() => toggleActivate()}>Cancel</Button>
                    </ModalFooter>
                </form>
            </Modal>
        </div>

    )
}

export default ModalEdit;
