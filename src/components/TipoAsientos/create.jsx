import Breadcrumb from '../common/breadcrumb';
import React, { Fragment, useState, useEffect } from 'react';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { GetListAccount, AddTipoAsientoCuenta } from '../TipoAsientos/Peticiones'
import { useForm, Controller } from "react-hook-form";
import SweetAlert from 'react-bootstrap-sweetalert';
import { useHistory } from "react-router-dom";




const CreateTipoAsiento = () => {



    const [options, setOptions] = useState([]);
    const { register, handleSubmit, errors, control } = useForm();
    const history = useHistory();

    const [sweetalert, setsweetalert] = useState({
        alert: null,
        show: false,
        basicTitle: '',
        basicType: "default"
    })

    const Confirm = () => {
        if (sweetalert.basicType === 'success') {
            history.push('/CuentasContables/List');
        } else {
            setsweetalert({ ...sweetalert, show: false })
        }
    }


    useEffect(() => {
        async function GetData() {
            var List = await GetListAccount()
            if (List != null) {
                setOptions(List)
            }
        }
        GetData();
    }, []);




    const onSubmit = data => {
        if (data !== '') {
            AddTipoAsientoCuenta(data).then(function (result) {
                if (!result) {
                    setsweetalert({ show: true, basicType: 'danger', basicTitle: 'Error' })
                } else {
                    setsweetalert({ show: true, basicType: 'success', basicTitle: 'Éxito' })
                }
            })


        } else {
            errors.showMessages();
        }
    };

    return (
        <Fragment>
            <SweetAlert
                show={sweetalert.show}
                type={sweetalert.basicType}
                title={sweetalert.basicTitle}
                onConfirm={() => Confirm()}
            >
            </SweetAlert >
            <Breadcrumb title="Crear tipo de asiento" parent="Gestión" />
            <div className="card">
                <div className="card-header"></div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-12">
                            <form className="needs-validation theme-form" noValidate="" onSubmit={handleSubmit(onSubmit)}>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Nombre del tipo de asiento</label>
                                    <div className="col-xl- col-sm-2">
                                        <input className="form-control" name="NombreTipoAsiento" type="text" ref={register({ required: true })} />
                                        <span>{errors.NombreTipoAsiento && 'Favor, introduzca el nombre del asiento.'}</span>
                                    </div>
                                </div>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Cuentas del asiento</label>
                                    <div className="col-sm-12 col-xl-6">
                                        <div id="prefetch">
                                            <Controller
                                                as={Typeahead}
                                                control={control}
                                                name="Cuentas"
                                                rules={{ required: true }}
                                                id="multiple-typeahead"
                                                clearButton
                                                multiple
                                                labelKey="cuentaNombre"
                                                options={options}
                                                defaultValue=""
                                                placeholder="Cuentas..."

                                            />
                                            <span>{errors.Cuentas && 'Favor digitar al menos una cuenta.'}</span>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-primary" type="submit">Guardar cuenta</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>


    )

}

export default CreateTipoAsiento;