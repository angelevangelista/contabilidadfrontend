import React, { Fragment, useState, useEffect } from 'react';
import Breadcrumb from '../common/breadcrumb';
import MUIDataTable from "mui-datatables";
import { GetListAsientos, GetAsiento } from '../TipoAsientos/Peticiones'
import ModalEdit from '../TipoAsientos/Edit'


const TipoAsientosList = () => {

    const [data, setData] = useState([{}]);
    const [activate, setActivate] = useState({ Estado: false, data: {}, id: null });


    const toggle = async (value) => {

        if (value != null) {
            var Account = await GetAsiento(value);
            setActivate({ Estado: true, data: Account, id: value });
        } else {
            setActivate({ Estado: (!activate), data: {} });
        }
    }

    const columns = [
        { name: "tipoAsientoNombre", label: "Tipo de asiento", options: { filter: true, sort: true, } },
        { name: "noCuentas", label: "No. cuentas asociadas", options: { filter: true, sort: true, } },
        {
            name: "estado", label: "Estado", type: 'boolean', options: {
                filter: true, sort: true, customBodyRender: (value, tableMeta, updateValue) => {
                    if (value == true) {
                        return <div>
                            <i className="fa fa-circle font-success f-12" />
                        </div>;
                    } else {
                        return <div><i className="fa fa-circle font-warning f-12" /></div>
                    }

                }
            },
        },
        {
            name: "tipoAsientoID", label: " ", options: {
                filter: false, sort: false, customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <i className="fa fa-trash" style={{ width: 35, fontSize: 16, padding: 11, color: '#e4566e' }}></i>
                            <button className="btn btn-success" type="submit" value="Submit" onClick={() => toggle(value)}>Editar</button>
                        </div>
                    )
                }
            }
        }
    ];


    const options = {
        selectableRows: 'none',
        selectableRowsOnClick: true
    };

    const GetData = async () => {
        var List = await GetListAsientos()
        if (List != null) {
            setData(List)
        }
    }

    useEffect(() => {
        GetData();
    }, []);



    return (
        <Fragment>
            <Breadcrumb title="Listado de cuentas" parent="Cuentas Contables" />
            <div className="container-fluid">
                <div className="row">
                    {/* <!-- Individual column searching (text inputs) Starts--> */}
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header">

                                {/*Aqui va el mensaje arriba del header con informaciones o detalles */}
                                {/*<h5>Individual column searching (text inputs) </h5><span>The searching functionality provided by DataTables is useful for quickly search through the information in the table - however the search is global, and you may wish to present controls that search on specific columns.</span>*/}
                            </div>
                            <div className="card-body">
                                <ModalEdit values={activate} toggleActivate={toggle} refresh={GetData} />
                                <MUIDataTable
                                    data={data}
                                    columns={columns}
                                    options={options}
                                />
                            </div>
                        </div>
                    </div>
                    {/* <!-- Individual column searching (text inputs) Ends--> */}
                </div>
            </div>
        </Fragment>
    );


}

export default TipoAsientosList;