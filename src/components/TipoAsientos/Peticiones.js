import axios from 'axios';

const GetListAccount = async () => {


    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/Utilities/CuentasContables");
        if (response.status === 200) {
            data = response.data;
        }
    } catch (err) {
        console.error(err)
    }

    return data;
}


const AddTipoAsientoCuenta = async (data) => {
    debugger
    const Cuenta = {
        Estado: data.Estado,
        nombreAsiento: data.NombreTipoAsiento,
        cuentasContables: data.Cuentas.map(function (item) { return item["cuentasContablesID"]; })

    };

    try {
        const response = await axios.post('http://localhost:53933/api/TipoAsientosContables', Cuenta);
        if (response.status === 200) {
            return true;
        }
    } catch (err) {
        return false;
    }
}

const GetListAsientos = async () => {


    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/TipoAsientosContables");
        if (response.status === 200) {
            data = response.data;
        }
    } catch (err) {
        console.error(err)
    }

    return data;
}

const GetAsiento = async (id) => {

    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/TipoAsientosContables/" + id)
        if (response.status === 200) {
            data = response.data[0];
        }
    } catch (err) {
        console.error(err)
    }
    return data;
}

const UpdateAsiento = async (data) => {
    const Cuenta = {
        Estado: data.Estado,
        nombreAsiento: data.NombreTipoAsiento,
        CambioCuentas: Array.isArray(data.Cuentas) ? true : false
    };

    if (Cuenta.CambioCuentas) {
        if (data.Cuentas.length > 0) {
            Cuenta.cuentasContables = data.Cuentas.map(function (item) { return item["cuentasContablesID"]; })
        } else {
            Cuenta.cuentasContables = [-2]
        }
    }

    try {
        const response = await axios.put('http://localhost:53933/api/TipoAsientosContables/' + data.ID, Cuenta);
        if (response.status === 200) {
            return true;
        }
    } catch (err) {
        return false;
    }
}

const GetCuentasAsientos = async (id) => {
    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/OrigenesCuentasAsientos/" + id)
        if (response.status === 200) {
            data = response.data;
        }
    } catch (err) {
        console.error(err)
    }
    return data;
}

const UpdateCuentasOrigen = async (data) => {

    try {
        const response = await axios.post('http://localhost:53933/api/OrigenesCuentasAsientos/', data);
        if (response.status === 200) {
            return true;
        }
    } catch (err) {
        return false;
    }

}

export { GetListAccount, AddTipoAsientoCuenta, GetListAsientos, GetAsiento, UpdateAsiento, GetCuentasAsientos, UpdateCuentasOrigen }





