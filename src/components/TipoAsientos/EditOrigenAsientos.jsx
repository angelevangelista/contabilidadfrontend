import React, { useState } from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';
import { UpdateCuentasOrigen, GetCuentasAsientos } from '../TipoAsientos/Peticiones'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';



const ModalEdit = ({ values, toggleActivate, refresh }) => {
    const [sweetalert, setsweetalert] = useState({
        alert: null,
        show: false,
        basicTitle: '',
        basicType: "default"
    })

    const [Cuentas, setCuentas] = useState({ tipoAsientoID: 0, asientoCuentas: [] });
    const [Abierto, setAbierto] = useState(false);

    const GetData = async () => {
        if (values.id != null) {
            var List = await GetCuentasAsientos(values.id)
            if (List != null) {
                setAbierto(true)
                setCuentas(List)
            }
        }
    }

    if (values.Estado && Abierto == false) {
        GetData()
    }


    const onSubmit = (event) => {
        event.preventDefault()

        UpdateCuentasOrigen(Cuentas).then(function (result) {
            if (result === true) {
                toggleActivate()
                setsweetalert({ show: true, basicType: 'success', basicTitle: 'Éxito' })
            } else {
                setsweetalert({ show: true, basicType: 'danger', basicTitle: 'Error' })

            }
        })
    };

    const toggle=()=>{
        setAbierto(false)
        toggleActivate()


    }

    const Confirm = () => {
        if (sweetalert.basicType === 'success') {
            refresh()
            setsweetalert({ ...sweetalert, show: false })

        } else {
            setsweetalert({ ...sweetalert, show: false })
        }
    }

    const Cambios = (event) => {

        let key = event.target.getAttribute("id");
        let update = event.target.value;
        var CuentaOld = { ...Cuentas }
        var modify = CuentaOld.asientoCuentas.findIndex(x => x.tipoAsientosCuentasID == key)
        CuentaOld.asientoCuentas[modify].cuentaOrigenID = parseInt(update);

        setCuentas(CuentaOld);

    }


    return (
        <div>
            <SweetAlert
                show={sweetalert.show}
                type={sweetalert.basicType}
                title={sweetalert.basicTitle}
                onConfirm={() => Confirm()}
            >
            </SweetAlert >
            <Modal isOpen={values.Estado} toggle={() => toggle()} >
                <form className="needs-validation theme-form" noValidate="" onSubmit={onSubmit}>
                    <ModalHeader toggle={() => toggle()}>Editar Origenes de cuentas</ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-md-9">
                                {Cuentas.asientoCuentas.map(function (d, idx) {
                                    return (
                                        <div className="form-group form-row" key={idx} >
                                            <label className="col-sm-4 col-form-label text-right" name="Cuenta">{d.nombreCuenta}</label>
                                            <div className="col-sm-4 col-form-label text-right">
                                                <select className="form-control btn-pill digits" id={d.tipoAsientosCuentasID} defaultValue={d.cuentaOrigenID} onChange={Cambios}>
                                                    <option value="1">Débito</option>
                                                    <option value="2">Crédito</option>
                                                    <option value="0">Ninguno</option>
                                                </select>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="col-md-9">
                                <div className="form-group form-row">
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">Guardar cambios</Button>
                        <Button color="secondary" onClick={() => toggle()}>Cancel</Button>
                    </ModalFooter>
                </form>
            </Modal>
        </div>

    )
}

export default ModalEdit;
