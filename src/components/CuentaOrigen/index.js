import React, { useState, useEffect } from 'react';
import axios from 'axios';


const CuentaOrigenList = (props,ref) => {

    const [data, setData] = useState([{}]);

    useEffect(() => {
        async function Peticion() {
            const result = await axios(
                'http://localhost:53933/api/Utilities/CuentaOrigen',
            );

            setData(result.data);
        }
        Peticion();
    }, []);

    return (
        <div>
            <select className="form-control digits" name="CuentaOrigenID"  defaultValue="1" ref={ref}>
                {data.map((item, index) => (
                    <option
                        key={index}
                        value={item.cuentaOrigenID}
                    >
                        {item.cuentaOrigenNombre}
                    </option>
                ))}
            </select>
        </div>

    )
};

export default React.forwardRef(CuentaOrigenList);

