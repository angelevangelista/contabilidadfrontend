import React, { Fragment, useState, useEffect } from 'react';
import Breadcrumb from '../common/breadcrumb';
import MUIDataTable from "mui-datatables";
import axios from 'axios';
import ModalEdit from '../CuentasContables/Edit'
import { GetListAccount, GetAccount } from '../CuentasContables/Peticiones'



const CuentaList = () => {
    const [data, setData] = useState([{}]);
    const [activate, setActivate] = useState({ Estado: false, data: { "nombreCuenta": "", "cuentaNomenclatura": "", "cuentaEstado": false, "cuentasContablesID": 0 }, id: null });


    const toggle = async (value) => {
        if (value != null) {
            var Account = await GetAccount(value);
            setActivate({ Estado: true, data: Account, id: value });
        } else {
            setActivate({ Estado: (!activate), data: {} });
        }
    }

    //#region configuraciones tabla

    const columns = [
        { name: "cuentaNombre", label: "Nombre cuenta", options: { filter: true, sort: true, } },
        { name: "cuentaNomenclatura", label: "Nomenclatura", options: { filter: true, sort: true, } },
        {
            name: "cuentaEstado", label: "Estado", type: 'boolean', options: {
                filter: true, sort: true, customBodyRender: (value, tableMeta, updateValue) => {
                    if (value == true) {
                        return <div>
                            <i className="fa fa-circle font-success f-12" />
                        </div>;
                    } else {
                        return <div><i className="fa fa-circle font-warning f-12" /></div>
                    }

                }
            },
        },
        {
            name: "cuentasContablesID", label: " ", options: {
                filter: false, sort: false, customBodyRender: (value, tableMeta, updateValue) => {
                    return (
                        <div>
                            <i className="fa fa-trash" style={{ width: 35, fontSize: 16, padding: 11, color: '#e4566e' }}></i>
                            <span onClick={() => toggle(value)}>
                                <i className="fa fa-pencil" style={{ width: 35, fontSize: 16, padding: 11, color: 'rgb(40, 167, 69)' }}
                                ></i>
                            </span>
                        </div>
                    )
                }
            }
        }
    ];

    const options = {
        selectableRows: 'none',
        selectableRowsOnClick: true
    };

    //#endregion

    //#region useEffect GetData to List

    const GetData = async () => {
        var List = await GetListAccount()
        if (List != null) {
            setData(List)
        }
    }

    useEffect(() => {
        GetData();
    }, []);


    //#endregion



    return (
        <Fragment>
            <Breadcrumb title="Listado de cuentas" parent="Cuentas Contables" />
            <div className="container-fluid">
                <div className="row">
                    {/* <!-- Individual column searching (text inputs) Starts--> */}
                    <div className="col-sm-12">
                        <div className="card">
                            <div className="card-header">

                                {/*Aqui va el mensaje arriba del header con informaciones o detalles */}
                                {/*<h5>Individual column searching (text inputs) </h5><span>The searching functionality provided by DataTables is useful for quickly search through the information in the table - however the search is global, and you may wish to present controls that search on specific columns.</span>*/}
                            </div>
                            <div className="card-body">
                                <ModalEdit values={activate} toggleActivate={toggle} refresh={GetData}/>
                                <MUIDataTable
                                    data={data}
                                    columns={columns}
                                    options={options}
                                />
                            </div>
                        </div>
                    </div>
                    {/* <!-- Individual column searching (text inputs) Ends--> */}
                </div>
            </div>
        </Fragment>
    );
};

export default CuentaList;