import React, { useEffect, useState } from 'react'
import { Fragment } from 'react'
import CuentaJerarquiaList from '../CuentaJerarquia/index'
import CuentaOrigenList from '../CuentaOrigen/index'
import CuentaTipoList from '../CuentaTipo/index'
import Breadcrumb from '../common/breadcrumb';
import {useForm} from 'react-hook-form'
import { AddAccount } from '../CuentasContables/Peticiones'
import SweetAlert from 'react-bootstrap-sweetalert';
import { useHistory } from "react-router-dom";



const Cuenta = () => {


  const { register, handleSubmit, errors } = useForm(); // initialise the hook
  const history = useHistory();

  const [sweetalert, setsweetalert] = useState({
    alert: null,
    show: false,
    basicTitle: '',
    basicType: "default"
  })

  const Confirm = () => {
    if (sweetalert.basicType === 'success') {
      history.push('/CuentasContables/List');
    } else {
      setsweetalert({ ...sweetalert, show: false })
    }
  }

  
  const onSubmit = data => {
    if (data !== '') {
      AddAccount(data).then(function (result) {
        if (!result) {
          setsweetalert({ show: true, basicType: 'danger', basicTitle: 'Error' })
        } else {
          setsweetalert({ show: true, basicType: 'success', basicTitle: 'Éxito' })
        }
      })
    } else {
      errors.showMessages();
    }
  };

  return (
    <Fragment>
      <SweetAlert
        show={sweetalert.show}
        type={sweetalert.basicType}
        title={sweetalert.basicTitle}
        onConfirm={() => Confirm()}
      >
      </SweetAlert >
      <Breadcrumb title="Crear cuenta" parent="Cuentas Contables" />
      <div className="card">
        <div className="card-header"></div>
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">
              <form className="needs-validation theme-form" noValidate="" onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Código</label>
                  <div className="col-xl-1 col-sm-2">
                    <input className="form-control" name="CuentaNomenclatura" type="number" ref={register({ required: true })} />
                    <span>{errors.CuentaNomenclatura && 'Favor, introduzca el cód.'}</span>
                  </div>
                </div>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Cuenta Control</label>
                  <div className="col-xl-1 col-sm-2">
                    <input className="form-control" name="CuentaRelaciones" type="text" placeholder="Control" ref={register({ required: true })} />
                    <span>{errors.CuentaRelaciones && 'Favor digitar cuenta control'}</span>
                  </div>
                </div>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Nombre</label>
                  <div className="col-xl-5 col-sm-4">
                    <input className="form-control" name="CuentaNombre" type="text" placeholder="Nombre cuenta" ref={register({ required: true })} />
                    <span>{errors.NombreCuenta && 'Favor digitar nombre de la cuenta'}</span>
                  </div>
                </div>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Jerarquía</label>
                  <div className="col-xl-4 col-sm-4">
                    <CuentaJerarquiaList ref={register} />
                  </div>
                </div>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Cuenta de origen</label>
                  <div className="col-xl-4 col-sm-4">
                    <CuentaOrigenList ref={register} />
                  </div>
                </div>
                <div className="form-group form-row">
                  <label className="col-sm-3 col-form-label text-right">Tipo de cuenta</label>
                  <div className="col-xl-4 col-sm-4">
                    <CuentaTipoList ref={register} />
                  </div>
                </div>
                <button className="btn btn-primary" type="submit">Guardar cuenta</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </Fragment >
  )
}

export default Cuenta