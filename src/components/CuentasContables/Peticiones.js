import axios from 'axios';


const AddAccount = async (data) => {

    const Cuenta = {
        CuentaNombre: data.CuentaNombre,
        CuentaNomenclatura: data.CuentaNomenclatura,
        CuentaOrigenID: data.CuentaOrigenID,
        CuentaJerarquiaID: data.CuentaJerarquiaID,
        CuentaTipoID: data.CuentaTipoID,
        CuentaRelaciones: data.CuentaRelaciones
    };

    try {
        const response = await axios.post('http://localhost:53933/api/CuentasContables', Cuenta);
        if (response.status === 200) {
            return true;
        }
    } catch (err) {
        return false;
    }
}

const GetListAccount = async () => {


    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/CuentasContables");
        if (response.status === 200) {
            data = response.data;
        }
    } catch (err) {
        console.error(err)
    }

    return data;
}

const GetAccount = async (id) => {

    var data = [{}];
    try {
        const response = await axios.get("http://localhost:53933/api/CuentasContables/" + id)
        if (response.status === 200) {
            data = response.data;
        }
    } catch (err) {
        console.error(err)
    }
    return data;
}

const UpdateAccount = async (data) => {
debugger
    const Cuenta = {
        CuentasContablesID: data.CuentasContablesID,
        CuentaNomenclatura: data.NomenclaturaCuenta,
        CuentaNombre: data.cuentaNombre,
        CuentaEstado: data.Estado,
        CuentaRelaciones: data.CuentaRelaciones
    };

    console.log(Cuenta);

    try {
        const response = await axios.put('http://localhost:53933/api/CuentasContables', Cuenta);
        if (response.status === 200) {
            return true;
        }
    } catch (err) {
        return false;
    }
}




export { AddAccount, GetListAccount, GetAccount, UpdateAccount } 