import React, { useState } from 'react';
import {useForm} from 'react-hook-form'
import SweetAlert from 'react-bootstrap-sweetalert';
import { UpdateAccount } from '../CuentasContables/Peticiones'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';



const ModalEdit = ({ values, toggleActivate, refresh }) => {

    const [sweetalert, setsweetalert] = useState({
        alert: null,
        show: false,
        basicTitle: '',
        basicType: "default"
    })


    const { register, handleSubmit, errors } = useForm(); // initialise the hook

    const onSubmit = campos => {
        debugger
        if (campos !== '') {
            campos.CuentasContablesID = values.id;
            UpdateAccount(campos).then(function (result) {

                if (result === true) {
                    toggleActivate()
                    setsweetalert({ show: true, basicType: 'success', basicTitle: 'Éxito' })
                } else {
                    setsweetalert({ show: true, basicType: 'danger', basicTitle: 'Error' })

                }
            })

        } else {
            errors.showMessages();
        }
    };

    const Confirm = () => {
        debugger
        if (sweetalert.basicType === 'success') {
            refresh()
            setsweetalert({ ...sweetalert, show: false })

        } else {
            setsweetalert({ ...sweetalert, show: false })
        }
    }




    return (
        <div>
            <SweetAlert
                show={sweetalert.show}
                type={sweetalert.basicType}
                title={sweetalert.basicTitle}
                onConfirm={() => Confirm()}
            >
            </SweetAlert >
            <Modal isOpen={values.Estado} toggle={() => toggleActivate()} >
                <form className="needs-validation theme-form" noValidate="" onSubmit={handleSubmit(onSubmit)}>
                    <ModalHeader toggle={() => toggleActivate()}>Editar cuenta</ModalHeader>
                    <ModalBody>
                        <div className="row">
                            <div className="col-md-9">
                                <div className="form-group form-row" >
                                    <div className="checkbox">
                                        <input type="checkbox" id="checkbox" name="Estado" defaultChecked={values.data.cuentaEstado} ref={register} />
                                        <label htmlFor="checkbox">Activado</label>
                                    </div>
                                </div>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Tipo de cuenta</label>
                                    <div className="col-xl-4 col-sm-4">
                                        <select className="form-control digits">
                                            <option>{values.data.cuentaTipo}</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Cuenta de origen</label>
                                    <div className="col-xl-6 col-sm-6">
                                        <select className="form-control digits">
                                            <option>{values.data.cuentaOrigen}</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Jerarquía</label>
                                    <div className="col-xl-4 col-sm-4">
                                        <select className="form-control digits">
                                            <option>{values.data.cuentaJerarquia}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="form-group form-row">
                                    <label className="col-sm-3 col-form-label text-right">Nombre</label>
                                    <input className="form-control col-sm-8" name="cuentaNombre" defaultValue={values.data.cuentaNombre} type="text" ref={register({ required: true })} />
                                    <span style={{ justifyContent: "center" }}>{errors.CuentaNombre && 'Favor digitar nombre de la cuenta'}</span>
                                </div>
                                <div className="form-group form-row" >
                                    <label className="col-sm-3 col-form-label text-right">Código</label>
                                    <input className="form-control col-sm-4" defaultValue={values.data.cuentaNomenclatura} name="NomenclaturaCuenta" type="number" ref={register({ required: true })} />
                                    <span style={{ justifyContent: "center" }}>{errors.NomenclaturaCuenta && 'Favor digitar nomenclatura'}</span>
                                </div>
                                <div className="form-group form-row" >
                                    <label className="col-sm-3 col-form-label text-right">Cuenta Control</label>
                                    <input className="form-control col-sm-4" defaultValue={values.data.cuentaRelaciones} name="CuentaRelaciones" type="number" ref={register({ required: true })} />
                                    <span style={{ justifyContent: "center" }}>{errors.CuentaRelaciones && 'Favor digitar la cuenta control'}</span>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" type="submit">Guardar cambios</Button>
                        <Button color="secondary" onClick={() => toggleActivate()}>Cancel</Button>
                    </ModalFooter>
                </form>
            </Modal>
        </div>

    )
}

export default ModalEdit;
