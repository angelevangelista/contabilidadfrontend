import {
    Home
} from 'react-feather';

export const MENUITEMS = [
    {
        title: 'Gestion', icon: Home, type: 'sub', active: false, children: [
            {
                title: "Cuentas Contables", type: 'sub', children: [
                    { path: '/CuentasContables/index', title: 'Agregar', type: 'link' },
                    { path: '/CuentasContables/List', title: 'Listado', type: 'link' }
                ]
            },
            {
                title: "Tipo de asientos", type: 'sub', children: [
                    { path: '/TipoAsientos/create', title: 'Crear', type: 'link' },
                    { path: '/TipoAsientos/List', title: 'Listado', type: 'link' },
                    { path: '/TipoAsientos/OrigenAsiento', title: 'Origenes de asientos', type: 'link' }
                ]
            }
        ]
    }
]
