import { combineReducers } from 'redux';
import Customizer from './customizer.reducer';
import userReducer from './userReducer'

const reducers = combineReducers({
    Customizer,
    usersList: userReducer

});

export default reducers;