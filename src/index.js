import React, { Fragment, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './index.scss';

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { ScrollContext } from 'react-router-scroll-4';
import * as serviceWorker from './serviceWorker';

// ** Import custom components for redux**
import { Provider } from 'react-redux';
import store from './store/store';
import App from "./components/app";

// Import custom Components 

import Default from './components/dashboard/defaultCompo/default';
import CrearCuentas from './components/CuentasContables/index';
import ListCuentas from './components/CuentasContables/List';
import CreateTipoAsientos from './components/TipoAsientos/create';
import ListTipoAsientos from './components/TipoAsientos/List';
import OrigenAsiento from './components/TipoAsientos/OrigenAsiento';





//firebase Auth
function Root() {
    useEffect(() => {
        const themeColor = localStorage.getItem('theme-color')
        const layout = localStorage.getItem('layout_version')
        document.getElementById("color").setAttribute("href", `${process.env.PUBLIC_URL}/assets/css/${themeColor}.css`);
        document.body.classList.add(layout);
    }, []);
    return (
        <div className="App">
            <Provider store={store}>
                <BrowserRouter basename={'/'}>
                    <ScrollContext>
                        <Switch>
                            <Fragment>
                                <App>
                                    {/* dashboard menu */}
                                    <Route exact path={`${process.env.PUBLIC_URL}/`} component={Default} />
                                    <Route path={`${process.env.PUBLIC_URL}/CuentasContables/index`} component={CrearCuentas} />
                                    <Route path={`${process.env.PUBLIC_URL}/CuentasContables/List`} component={ListCuentas} />
                                    <Route path={`${process.env.PUBLIC_URL}/TipoAsientos/Create`} component={CreateTipoAsientos} />
                                    <Route path={`${process.env.PUBLIC_URL}/TipoAsientos/List`} component={ListTipoAsientos} />
                                    <Route path={`${process.env.PUBLIC_URL}/TipoAsientos/OrigenAsiento`} component={OrigenAsiento} />
                                </App>
                            </Fragment>
                        </Switch>
                    </ScrollContext>
                </BrowserRouter>
            </Provider>
        </div>
    );
}

ReactDOM.render(<Root />, document.getElementById('root'));

serviceWorker.unregister();